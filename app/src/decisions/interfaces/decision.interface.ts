import { Document } from 'mongoose';

export interface Decision extends Document {
    readonly title: string;
    readonly content: string;
    readonly userId: string;
}
