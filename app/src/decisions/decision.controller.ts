import { Controller, Get, Post, Body, Param } from '@nestjs/common';
import { CreateDecisionDto } from './dto/create-decision.dto';
import { DecisionsService } from './decision.service';
import { Decision as DecisionInterface } from './interfaces/decision.interface';

@Controller('decisions')
export class DecisionController {
    constructor(private readonly decisionsService: DecisionsService) { }

    @Post('/')
    async create( @Body() createPostDto: CreateDecisionDto) {
      console.log('decision controller POST');
        this.decisionsService.create(createPostDto).then(_data => {
          console.log(_data);
        });
    }

    @Get()
    async findAll(): Promise<DecisionInterface[]> {
      console.log('decision controller GET ', this.decisionsService.findAll());
        return this.decisionsService.findAll()
    }
}
