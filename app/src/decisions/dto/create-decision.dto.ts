export class CreateDecisionDto {
    readonly title: string;
    readonly content: string;
    readonly userId: string;
}
