import { Model } from 'mongoose';
import { Inject, Injectable } from '@nestjs/common';

import { Decision } from './interfaces/decision.interface';
import { CreateDecisionDto } from './dto/create-decision.dto';
import { DECISION_MODEL_PROVIDER } from '../constants';

@Injectable()
export class DecisionsService {
    constructor(
        @Inject(DECISION_MODEL_PROVIDER) private readonly decisionModel: Model<Decision>) { }

    async create(CreateDecisionDto: CreateDecisionDto): Promise<Decision> {
        const createdDecision = new this.decisionModel(CreateDecisionDto);
        return await createdDecision.save();
    }

    async findAll(): Promise<Decision[]> {
        return await this.decisionModel.find().exec();
    }
}
