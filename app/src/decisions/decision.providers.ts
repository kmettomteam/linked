import { Connection } from 'mongoose';

import { DecisionSchema } from './decision.schema';
import { DECISION_MODEL_PROVIDER, DB_PROVIDER } from '../constants';

export const decisionsProviders = [
    {
        provide: DECISION_MODEL_PROVIDER,
        useFactory: (connection: Connection) => connection.model('Decision', DecisionSchema),
        inject: [DB_PROVIDER],
    },
];
