import { Controller, Get, Post , Param} from '@nestjs/common';
import { AppService } from './app.service';

let decisionTestData = {
  name: 'test decision',
  content: 'test content of an decision, this decision is an agreement of the parties that xxx',
  id: null,
  status: { published: false, finished: false, executed: false},
  consensus: {
    type: 'vote', // allagree, bypercent

  },
  results: {
    agreed: [],
    disagreed: [],
    nooppinion: [],
  },
  participants: [
    {id: 1, name: 'Mat'},
    {id: 2, name: 'Suzan'},
    {id: 3, name: 'Karl'},
  ]
}

@Controller()
export class AppController {
  constructor(private readonly appService: AppService) {}

  // @Get()
  // getHello(): string {
  //   console.log('get - 0')
  //   // return this.appService.getHello();
  // }

  @Get('decision/:id')
  getDecision(@Param('id') _id): object {
    console.log('getDecision - ' , _id );
    decisionTestData.id = _id;
    return decisionTestData;
  }

  @Post('decision/new')
  createDecision(): object {
    decisionTestData.id = 783;
    return decisionTestData;
  }
}
