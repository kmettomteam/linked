import { Component } from '@angular/core';
// import {  } from './decision/decision.component';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent {
  title = 'linked-ui';
}
