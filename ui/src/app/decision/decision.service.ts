import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
// import { Observable, of } from 'rxjs';

import {environment} from '../../environments/environment';

// export interface Deisions {
//     id: Number;
//     name: String;
//     description: String;
// }

@Injectable({
  providedIn: 'root'
})
export class DecisionService {

  constructor( private http:HttpClient ) { }

  getDecision(_id) {
    console.log('get.decisions', _id);
    return this.http.get(environment.api + '/decision/' + _id)
    .toPromise()
    .then(response => {
      return response
      // console.log(response)

    }).catch(error => {
      console.log(error)
    })

  }

  createDecision(){
    console.log('create.decisions')
    return this.http.post(environment.api + '/decision/new', { decision: '1' } )
    .toPromise()
    .then(response => {
      return response
    }).catch(error => {
      console.log(error)
    })


    // .pipe(
    //   catchError(this.handleError<Deisions[]>('decisions', []))
    // );
  }

}
