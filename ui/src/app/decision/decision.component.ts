import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { DecisionService } from './decision.service';

@Component({
  selector: 'app-decision',
  templateUrl: './decision.component.html',
  styleUrls: ['./decision.component.scss']
})

export class DecisionComponent implements OnInit {
  public activeMenuTab;
  public decisionId;
  public decisionData;


  constructor(private _Activatedroute:ActivatedRoute, private _DecisionService: DecisionService) {
    // this.activeMenuTab = null;
    // this.decisionId = null;
    // this.decisionData = null;

  }

  public menuTabChange(item){
    this.activeMenuTab = item;
  }

  ngOnInit(): void {

    this._Activatedroute.paramMap.subscribe(params => {
      this.decisionId = params.get('id');
      this.activeMenuTab = 'overview';
      if(this.decisionId === 'new'){
        this._DecisionService.createDecision().then(_data => {
          this.decisionData = _data;
        });;
      }else{
        this._DecisionService.getDecision(this.decisionId).then(_data => {
          console.log(_data);
          this.decisionData = _data;
        });
      }

    });

  }

}
