import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { HttpClientModule } from '@angular/common/http';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';

import { DecisionComponent } from './decision/decision.component';
import { HomeComponent } from './home/home.component';
import { ParticipantComponent } from './participant/participant.component';

@NgModule({
  imports: [
    BrowserModule,
    HttpClientModule,
    AppRoutingModule,
  ],
  declarations: [
    AppComponent,
    DecisionComponent,
    HomeComponent,
    ParticipantComponent
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
