import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import {HomeComponent} from './home/home.component.js' ;
import {DecisionComponent} from './decision/decision.component.js' ;
import {ParticipantComponent} from './participant/participant.component.js' ;


const routes: Routes = [
  { path: '', component: HomeComponent },
  { path: 'decision/:id', component: DecisionComponent },
  { path: 'participant', component: ParticipantComponent },
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
