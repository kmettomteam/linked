import { enableProdMode } from '@angular/core';
import { platformBrowserDynamic } from '@angular/platform-browser-dynamic';

import { AppModule } from './app/app.module';
import { environment } from './environments/environment';

import Web3 from 'web3' ;
// var Web3 = require('web3');
// let web3 = new Web3(Web3.givenProvider || "ws://localhost:8545");
// console.log('x' , web3);


// Initialize Web3
let web3;
    if (typeof web3 !== 'undefined') {
      web3 = new Web3(web3.currentProvider);
    } else {
      web3 = new Web3(new Web3.providers.HttpProvider('http://localhost:7545'));
    }

    // Set Account
    web3.eth.defaultAccount = web3.eth.accounts[0];

    // Set Contract Abi
    // Add Your Contract ABI here!!!
    var contractAbi = [
	{
		"constant": false,
		"inputs": [],
		"name": "Elect",
		"outputs": [],
		"payable": false,
		"stateMutability": "nonpayable",
		"type": "function"
	},
	{
		"constant": false,
		"inputs": [
			{
				"name": "_name",
				"type": "string"
			}
		],
		"name": "setCandidate",
		"outputs": [],
		"payable": false,
		"stateMutability": "nonpayable",
		"type": "function"
	},
	{
		"constant": true,
		"inputs": [],
		"name": "candidateName",
		"outputs": [
			{
				"name": "",
				"type": "string"
			}
		],
		"payable": false,
		"stateMutability": "view",
		"type": "function"
	}
];

    // Set Contract Address
    var contractAddress = '0xD70bb0D25D34f8924E22F0Bb00076acf516bBB7d'; // Add Your Contract address here!!!

    // Set the Contract
    const contract = new web3.eth.Contract( contractAbi , contractAddress );
    contract.methods.candidateName().call().then( d => {

      console.log('1 get' , d);

    });

    setTimeout(() => {

      contract.methods.setCandidate("kkt").send( { from: "0x199CD563B7a0E5e8c05830e1726F5727BeE70511" } ).then(d=>{
        console.log('2 set' , d);

      });

    }, 200);



    setTimeout(() => {
    contract.methods.candidateName().call().then( d => {

      console.log('3 get' , d);

    });
  }, 1500);

    // Display Candidate Name
    // contract.methods.candidateName.call(function(_candidateName){
    //   console.log(_candidateName);
    // });

    // contract.methods.setCandidate("kokot 2");

    // (function(err, candidateName) {
    //   console.log(candidateName);
    // });

    // Change the Candidate Name
    // $('form').on('submit', function(event) {
    //   event.preventDefault();
    //   contract.setCandidate($('input').val());
    // })


if (environment.production) {
  enableProdMode();
}

platformBrowserDynamic().bootstrapModule(AppModule)
  .catch(err => console.error(err));
